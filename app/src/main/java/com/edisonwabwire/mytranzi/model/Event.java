package com.edisonwabwire.mytranzi.model;

public class Event {
    private String evtName;
    private String evtOrg;
    private int evtImage;
    private String evtRoutes;
    private String evtDate;

    public Event(String evtName, String evtOrg, int evtImage, String evtRoutes, String evtDate) {
        this.evtName = evtName;
        this.evtOrg = evtOrg;
        this.evtImage = evtImage;
        this.evtRoutes = evtRoutes;
        this.evtDate = evtDate;
    }

    public String getEvtName() {
        return evtName;
    }

    public void setEvtName(String evtName) {
        this.evtName = evtName;
    }

    public String getEvtOrg() {
        return evtOrg;
    }

    public void setEvtOrg(String evtOrg) {
        this.evtOrg = evtOrg;
    }

    public int getEvtImage() {
        return evtImage;
    }

    public void setEvtImage(int evtImage) {
        this.evtImage = evtImage;
    }

    public String getEvtRoutes() {
        return evtRoutes;
    }

    public void setEvtRoutes(String evtRoutes) {
        this.evtRoutes = evtRoutes;
    }

    public String getEvtDate() {
        return evtDate;
    }

    public void setEvtDate(String evtDate) {
        this.evtDate = evtDate;
    }
}
