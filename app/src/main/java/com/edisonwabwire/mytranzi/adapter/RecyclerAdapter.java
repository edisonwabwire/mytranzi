package com.edisonwabwire.mytranzi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edisonwabwire.mytranzi.R;
import com.edisonwabwire.mytranzi.model.Event;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private static OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    private static List<Event> events;
    private Context context;

    public RecyclerAdapter(List<Event> events, Context context) {
        this.events = events;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view,viewGroup,false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Event event = events.get(i);
        viewHolder.txtEvent.setText(event.getEvtName());
        viewHolder.txtDetail.setText(event.getEvtOrg());
        viewHolder.txtRoute.setText(event.getEvtRoutes());
        viewHolder.txtDate.setText(event.getEvtDate());
        viewHolder.txtEventImage.setImageResource(event.getEvtImage());

    }

    @Override
    public int getItemCount() {

        return events.size();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtEvent;
        public TextView txtDetail;
        public TextView txtDate;
        public TextView txtRoute;
        public ImageView txtEventImage;



        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            txtEvent = itemView.findViewById(R.id.txtEventName);
            txtDetail = itemView.findViewById(R.id.txtEventDetail);
            txtEventImage = itemView.findViewById(R.id.eventImage);
            txtRoute = itemView.findViewById(R.id.txtRoutes);
            txtDate = itemView.findViewById(R.id.txtEventDate);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });

        }
    }

}
