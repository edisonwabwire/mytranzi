package com.edisonwabwire.mytranzi.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.edisonwabwire.mytranzi.R;
import com.edisonwabwire.mytranzi.adapter.RecyclerAdapter;
import com.edisonwabwire.mytranzi.model.Event;

import java.util.ArrayList;
import java.util.List;

import static com.edisonwabwire.mytranzi.adapter.RecyclerAdapter.OnItemClickListener;


public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerViewAdapter;
    private List<Event> eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView =  findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        eventList = new ArrayList<>();
        recyclerViewAdapter = new RecyclerAdapter(eventList,MainActivity.this);
        recyclerView.setAdapter(recyclerViewAdapter);

        prepareEventData();

        recyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
//                String event = eventList.get(position).getEvtName();
//                Toast.makeText(MainActivity.this, event + " was clicked!", Toast.LENGTH_SHORT).show();
                Context context = itemView.getContext();
                Intent intent = new Intent(context,PrimaryActivity.class);
                context.startActivity(intent);
            }
        });
    }

    private void prepareEventData(){
        Event mEvent = new Event
                ("Burna Boy Concert", "Talent Africa", 0,
                        "4 Routes Available","12th, july 2019");
        eventList.add(mEvent);
        mEvent = new Event
                ("Nyege Ngege", "Fenon Events", 0,
                        "2 Routes Available","12th, july 2019");
        eventList.add(mEvent);
        mEvent = new Event
                ("Phaneroo Night", "Phaneroo Ministries", 0,
                        "4 Routes Available","12th, july 2019");
        eventList.add(mEvent);
        mEvent = new Event
                ("Uganda Cranes", "Talent Africa", 0,
                        "4 Routes Available","12th, july 2019");
        eventList.add(mEvent);

        recyclerViewAdapter.notifyDataSetChanged();

    }
}
