package com.edisonwabwire.mytranzi.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.edisonwabwire.mytranzi.R;
import com.edisonwabwire.mytranzi.adapter.DetailsAdapter;
import com.edisonwabwire.mytranzi.model.Details;

import java.util.ArrayList;
import java.util.List;

public class PrimaryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DetailsAdapter recyclerViewAdapter;
    private List<Details> eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary);

        recyclerView =  findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(PrimaryActivity.this));

        eventList = new ArrayList<>();
        recyclerViewAdapter = new DetailsAdapter(eventList,PrimaryActivity.this);
        recyclerView.setAdapter(recyclerViewAdapter);

        prepareEventData();


    }

    private void prepareEventData(){
        Details mEvent = new Details
                ("Burna Boy Concert", "Talent Africa", 0,
                        "4 Routes Available","12th, july 2019");
        eventList.add(mEvent);
        mEvent = new Details("Nyege Ngege", "Fenon Events", 0,
                "2 Routes Available","12th, july 2019");
        eventList.add(mEvent);
        mEvent = new Details("Phaneroo Night", "Phaneroo Ministries", 0,
                "4 Routes Available","12th, july 2019");
        eventList.add(mEvent);
        mEvent = new Details("Uganda Cranes", "Talent Africa", 0,
                "4 Routes Available","12th, july 2019");
        eventList.add(mEvent);

        recyclerViewAdapter.notifyDataSetChanged();

    }
}
